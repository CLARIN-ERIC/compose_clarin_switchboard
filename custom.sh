#!/bin/bash
set -e

main () {

    VERBOSE=0
    HELP=0
    ERROR=0

    while [ $# -gt 0 ]
        do
        key="$1"
        case $key in
            "")
                ;;
            -h|--help)
                HELP=1
                ;;
            -v|--verbose)
                VERBOSE=1
                ;;
            *)
                ARGS_ARRAY+=("$key")
                ;;
        esac
        shift
    done

    if [ "${VERBOSE}" -eq 1 ]; then
        set -x
    fi

    if [ -n "${ARGS_ARRAY[0]}" ]; then
        case ${ARGS_ARRAY[0]} in
            apply-update)
                apply_update
                exit 0
                ;;
            *)
                fatal "Subcommand not available in $(basename "${0}") script!"
                ;;
        esac
    fi

    if [ "${HELP}" -eq 1 ]; then
        echo "    apply-update              Apply environment changes need for a new Switchboard version"
        echo ""
        if [ "${ERROR}" -eq 0 ]; then
            exit 0
        else
            exit 1
        fi
    fi
}

apply_update ( ) {
    apply_update_2_3_0
}

apply_update_2_3_0 ( ) {
    set_compose_dir
    if [ -e "$COMPOSE_DIR"/.env ]; then
        cd "$COMPOSE_DIR"
        if ! grep -q "_APP_URLRESOLVER_MAX_CACHE_ENTRIES" .env; then
            echo ".env file needs update!"
            text=$(grep "_APP_URLRESOLVER_MAX_CACHE_ENTRIES" .env-template)
            line_number=$(grep -n "_APP_URLRESOLVER_MAX_CACHE_ENTRIES" .env-template | cut -d : -f 1)
            echo "Adding: \"${text}\" to line ${line_number}:"
            sed_command="sed"
            if [[ $(uname) == 'Darwin' ]]; then
            echo "OSX platform detected. Using gsed."
               sed_command="gsed"
            fi
            TMP_SED=$(${sed_command} "${line_number}i${text}" .env)
            echo "$TMP_SED" > .env
            unset TMP_SED
            echo ".env file updated!"
        else
            echo ".env file already updated!"
        fi
    fi
}

set_compose_dir ( ) {
    COMPOSE_DIR="clarin"
    if readlink "$0"  >/dev/null 2>&1; then
        COMPOSE_DIR=$(dirname "$(readlink "$0")")/$COMPOSE_DIR
    else
        COMPOSE_DIR=$(dirname "${BASH_SOURCE[0]}")/$COMPOSE_DIR
    fi
}

main "$@"; exit